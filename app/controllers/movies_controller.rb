class MoviesController < ApplicationController
  before_action :find_movie, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index, :show]  
  def index
    @movies = Movie.all
  end

  def show
  end

  def create
    @movie = Movie.new(movie_params)
    if @movie.save
      redirect_to @movie
    else
      render :new
    end
  end

  def new
    @movie = Movie.new
  end


private

def find_movie
  @movie = Movie.find(params[:id])
end

def movie_params
  params.require(:movie).permit(:title, :description)
end
#:director, :actors, :year,

end
