class Movie < ActiveRecord::Base

  has_attached_file :image, styles: {
    thumb: '200x300>',
    medium: '400x600#'
  }

  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
end
